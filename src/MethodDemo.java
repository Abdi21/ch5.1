public class MethodDemo {
	public static void main(String [] args) {
		System.out.println(absolute(-3));
	}
	public static int absolute(int num) {
		return (num>=0)?num:(-1*num);
	}
}